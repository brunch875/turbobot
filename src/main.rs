mod whitelist;
mod commands;

use std::env;
use commands::mimic::*;
use commands::minecraft::*;
use commands::feckinmad::*;
use std::collections::HashSet;
use serenity::{
    model::{channel::Message, id::UserId},
    prelude::*,
    framework::standard::{
        Args,
        StandardFramework,
        CommandResult,
        CommandGroup,
        DispatchError,
        help_commands,
        HelpOptions,
        macros::{group, help, hook},
    },
};


#[help]
async fn help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>
    ) -> CommandResult {
    help_commands::with_embeds(context, msg, args, help_options, groups, owners).await?;
    Ok(())
}


#[group]
#[commands(lick, play, nyan, avatar)]
struct Mimic;

#[group]
#[prefixes(mc, minecraft)]
#[commands(whitelist, unwhitelist, stop, restart, players, rcon)]
struct Minecraft;

#[group]
#[commands(server)]
struct Feckinmad;

struct Handler;
impl EventHandler for Handler { }

fn is_insult(words: &[&str]) -> bool {
    let words_lowercase: Vec<String> = words.iter().map(|item| item.to_lowercase()).collect();
    let words_lowercase: Vec<&str> = words_lowercase.iter().map(AsRef::as_ref).collect();
    matches!(&words_lowercase[..],
             ["u"] | ["you"] | ["no", "u"] |
             ["you're", "a", ..] | ["you", "are", "a", ..] | [ "ura", .. ] |
             ["you're", "an", ..] | ["you", "are", "an", ..] | ["ur", "a", ..] | ["ur", "an"] |
             ["your", _] | ["urmom"] | ["ur", _] |
             ["you're", _] | ["you", "are", _])
}

async fn reply(ctx: &Context, src_msg: &Message, msg: &str) -> serenity::Result<Message> {
    src_msg.channel_id.say(&ctx.http, msg).await
}

#[hook]
async fn on_normal_message(ctx: &Context, msg: &Message) {
    println!("Normal message: {}", msg.content);
    if msg.author.bot {
        return; //Avoid feedback loops
    }

    let words = msg.content.split_whitespace().collect::<Vec<&str>>();

    let response = match &words[..] {
        [ "/whitelist", "reload" ] => Some("You have to write that in minecraft, not here; ya twit!"),
        insult if is_insult(insult) => Some("no u"),
        _ => None,
    };

    if let Some(response) = response {
        if let Err(e) = reply(ctx, msg, response).await { 
            eprintln!("Unable to respond: {:?}", e);
        }
    }

}

#[hook]
async fn on_unrecognised_command(ctx: &Context, msg: &Message, cmd_name: &str) {
    if let Err(reason) = reply(ctx, msg, &format!("Wotsa `{}`? Can I eat it?", cmd_name)).await {
        eprintln!("Unable to respond to command {}: {:?}", cmd_name, reason);
    }
}

#[hook]
async fn on_dispatch_error(ctx: &Context, msg: &Message, error: DispatchError, _command_name: &str) {
    eprintln!("Dispatch error! {:?} -> {:?}", error, msg.content);

    let response = match error {
        DispatchError::NotEnoughArguments { min, given } => Some(format!("I need `{}` parameter(s) for that command and you only gave me `{}`, you greedy bastard", min, given)),
        DispatchError::TooManyArguments { max, given } => Some(format!("Woooaah! I only needed `{}` parameter(s) for that command and you flooded me with `{}`", max, given)),
        _ => None,
    };

    if let Some(response) = response {
        if let Err(e) = reply(ctx, msg, &response).await {
            eprintln!("Unable to respond on dispatch error: {:?}", e); 
        }
    }
}

#[tokio::main]
async fn main() {

    let token = env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN not set");

    // TODO: Implement on mentioning bot with @TurboBot
    let framework = StandardFramework::new()
        .bucket("five_sec_cooldown", |b| b.delay(5)).await
        .configure(|c| c.no_dm_prefix(true).prefix("&"))
        .unrecognised_command(on_unrecognised_command)
        .normal_message(on_normal_message)
        .on_dispatch_error(on_dispatch_error)
        .group(&FECKINMAD_GROUP)
        .group(&MINECRAFT_GROUP)
        .group(&MIMIC_GROUP)
        .help(&HELP);

    let mut client = Client::builder(token, GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT)
        .framework(framework)
        .event_handler(Handler)
        .await.expect("Err creating client");

    client.start().await.expect("Unable to start the client");
}
