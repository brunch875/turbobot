use serde::{Deserialize, Serialize};

use md5::{Md5, Digest};
use uuid::Uuid;
use std::error::Error;
use std::fs::File;
use std::io::{Write};
use std::path::Path;

fn hyphenated(uuid: &Uuid) -> String {
    uuid.hyphenated().to_string()
}

#[derive(Serialize, Deserialize)]
#[serde(remote="Uuid")]
struct UuidStr(
    #[serde(getter="hyphenated")]
    String
);

impl From<UuidStr> for Uuid {
    fn from(def: UuidStr) -> Uuid {
        Uuid::parse_str(&def.0).unwrap_or_default()
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct User {
    #[serde(with="UuidStr")]
    uuid: Uuid,
    name: String,
}

impl User {
    fn new(username: &str) -> Self {
        let mut value = "OfflinePlayer:".to_owned();
        value.push_str(username);

        let mut hasher = Md5::new();
        hasher.update(value.as_bytes());
        let mut hash: [u8;16] = hasher.finalize().into();
        
        // This has been taken as is from minecraft itself
        hash[6] = hash[6] & 0x0F | 0x30;
        hash[8] = hash[8] & 0x3F | 0x80;

        User {
            name: username.to_owned(),
            uuid: Uuid::from_bytes(hash),
        }
    }
}


#[derive(Debug)]
/// Users whitelisted in the minecraft server
/// The lifecycle of this object is `load` -> `register` -> `commit`
pub struct Whitelist<P: AsRef<Path>> {
    /// File the whitelist was loaded from. It will also be saved here when commited
    path: P,
    users: Vec<User>,
}

impl<P: AsRef<Path> + Copy> Whitelist<P> {
    /// Constructs a whitelist after loading it from `path`
    pub fn load(path: P) -> Result<Whitelist<P>, Box<dyn Error>> {
        let file = File::open(path)?;

        let users = Whitelist {
            users: serde_json::from_reader(&file)?,
            path,
        };

        Ok(users)
    }

    ///Checks whether an user is registered with the same name
    fn contains(&self, user: &str) -> bool {
        self.users.iter().any(|entry| entry.name == user)
    }

    /// Adds a new user to the whitelist, calculating its **offline** uuid
    /// If the user is already registered, returns Err
    pub fn register(&mut self, user: &str) -> Result<(), ()> {
        if self.contains(user) {
            Err(())
        } else {
            self.users.push(User::new(user));
            Ok(())
        }
    }

    /// Removes an existing user from the whitelist
    /// If the user isn't registered, returns Err
    pub fn unregister(&mut self, user: &str) -> Result<(), ()> {
        let old_length = self.users.len();
        self.users.retain(|whitelisted| whitelisted.name != user);
        let new_length = self.users.len();

        if old_length == new_length {
            Err(())
        } else {
            Ok(())
        }
    }

    /// Saves the whitelist back to the file it was loaded from
    pub fn commit(self) -> Result<(), Box<dyn Error>> {
        let mut file = File::create(self.path)?;
        serde_json::to_writer_pretty(&file, &self.users)?;
        file.write_all(b"\n")?;
        file.flush()?;
        Ok(())
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn user_creation_md5() {
        let user = super::User::new("SageOfSixPaths");
        assert_eq!("c2fb9473-456f-3060-819a-9ba4ef472868", super::hyphenated(&user.uuid));
    }
}
