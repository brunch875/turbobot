use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    Args, CommandResult,
    macros::command,
};

use std::process::Command;

use rcon::Connection;

mod whitelist;
use whitelist::{register_user, unregister_user, Success, Failure};

#[command]
#[description = "Runs an arbitrary rcon command. The help rcon command lists available commands"]
#[min_args(1)]
pub async fn rcon(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let response = match rcon_exec(args.message()) {
        Ok(server_info) => server_info,
        Err(_) => String::from("Couldn't execute rcon command 😵"),
    };
    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after rcon command: {:?}", reason);
    }

    Ok(())
}


#[command]
#[description = "Lists the players currently playing in the minecraft server"]
#[num_args(0)]
pub async fn players(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let response = match rcon_exec("list") {
        Ok(server_info) => server_info,
        Err(_) => String::from("I have no idea"),
    };
    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after attempting listing players: {:?}", reason);
    }

    Ok(())
}

// TODO: These two functions have grown nearly identically. Change to Success { Updated, Unchanged }
// and merge
#[command]
#[description = "Whitelists a new player on the minecraft server"]
#[usage="<user>"]
#[num_args(1)]
pub async fn whitelist(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user: String = args.single()?;

    let response = match register_user(&user) {
        Ok(Success::Updated) => format!("Ok! Registered `{}`", user),
        Ok(Success::Unchanged) => format!("`{}` is already whitelisted, you silly!", user),
        Err(Failure::Load) => String::from("I can't fetch the whitelist :("),
        Err(Failure::Commit) => format!("I tried to add `{}`, but I couldn't save the whitelist :(", user),
        Err(Failure::RconReload) => format!("Ok! Registered `{}` but rcon reload failed. Ask for someone inside the server to type `/whitelist reload`. Restarting the server also works", user),
    };


    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after attempting whitelist: {:?}", reason);
    }

    Ok(())
}

#[command]
#[description = "Removes the whitelist of an existing player from the minecraft server"]
#[usage="<user>"]
#[num_args(1)]
pub async fn unwhitelist(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user: String = args.single()?;

    let response = match unregister_user(&user) {
        Ok(Success::Updated) => format!("Ok! Removed `{}`", user),
        Ok(Success::Unchanged) => format!("`{}` isn't whitelisted, you silly!", user),
        Err(Failure::Load) => String::from("I can't fetch the whitelist :("),
        Err(Failure::Commit) => format!("I tried to remove `{}`, but I couldn't save the whitelist :(", user),
        Err(Failure::RconReload) => format!("Ok! Removed `{}`, but rcon reload failed. Ask for someone inside the server to type `/whitelist reload`. Restarting the server also works", user),
    };


    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after attempting whitelist: {:?}", reason);
    }

    Ok(())
}

#[command]
#[description = "Restarts the minecraft server"]
pub async fn restart(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let response = match Command::new("sudo").args(["systemctl", "restart", "minecraft-server"]).spawn() {
        Ok(_) => "Ok! Server should be restarting now",
        Err(_) => "Ooops... it didn't work. I hope I didn't break anything :x",
    };

    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after attempting minecraft restart: {:?}", reason);
    }

    Ok(())
}

#[command]
#[description = "Stops the minecraft server"]
pub async fn stop(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let response = match Command::new("sudo").args(["systemctl", "stop", "minecraft-server"]).spawn() {
        Ok(_) => "Ok! Stopped the server",
        Err(_) => "Ooops... it didn't work. I hope I didn't break anything :x",
    };

    if let Err(reason) = msg.channel_id.say(&ctx.http, response).await {
        eprintln!("Couldn't respond with success value after attempting minecraft stop: {:?}", reason);
    }

    Ok(())
}

fn rcon_exec(cmd: &str) -> Result<String, ()> {
    let addr = &"127.0.0.1:25575".parse().map_err(|_| ())?;
    let mut connection = Connection::new(addr, "puddipuddi").map_err(|_| ())?;
    connection.request(cmd).map_err(|_| ())
}
