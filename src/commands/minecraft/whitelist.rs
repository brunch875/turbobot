use crate::whitelist::Whitelist;

pub enum Success { Updated, Unchanged }
pub enum Failure { Load, Commit, RconReload }

/// Registers a new user in the whitelist and reloads it
pub fn register_user(user: &str) -> Result<Success, Failure> {
    whitelist_operation(|whitelist| whitelist.register(user).is_ok())
}

/// Removes a user from the whitelist and reloads it
pub fn unregister_user(user: &str) -> Result<Success, Failure> {
    whitelist_operation(|whitelist| whitelist.unregister(user).is_ok())
}

/// Loads the server's whitelist, performs the operation passed as argument, and reloads the
/// whitelist.
///
/// The passed function must return `false` if no action was necessary to perform the operation
fn whitelist_operation<F>(operation: F) -> Result<Success, Failure>
    where F: Fn(&mut Whitelist<&str>) -> bool {
    let mut whitelist = Whitelist::load("/srv/minecraft-server/whitelist.json")
                                    .map_err(|_| Failure::Load)?;

    if !operation(&mut whitelist) {
        return Ok(Success::Unchanged);
    }

    if whitelist.commit().is_err() {
        return Err(Failure::Commit);
    }

    super::rcon_exec("whitelist reload").map(|_| Success::Updated).map_err(|_| Failure::RconReload)
}
