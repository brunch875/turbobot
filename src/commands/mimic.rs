use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    Args, CommandResult,
    macros::command,
};

#[command]
pub async fn lick(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Eeeewww! No way! Nasty!").await?;
    Ok(())
}

#[command]
pub async fn play(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id.say(&ctx.http, "🎵 Smoke is flying everywhere 🎶🎵 I'm getting the feeling it's the end for you and me 🎵").await?;
    Ok(())
}

#[command]
pub async fn rank(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id.say(&ctx.http, "You're the prettiest and the bestest to my eyes").await?;
    Ok(())
}

#[command]
pub async fn nyan(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id.say(&ctx.http, "😸 Nyan neko NYAA~~ 😽").await?;
    Ok(())
}

#[command]
pub async fn avatar(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id.say(&ctx.http, "Mine is way cooler 🙄").await?;
    Ok(())
}
