use std::borrow::Cow;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

use steam_server_query::{A2SInfo, QueryError, SteamQuerier};

const FECKINMAD_SERVER: &str = "37.122.208.248:27017";

#[command]
#[bucket = "five_sec_cooldown"]
#[description = "Displays map and players on the feckinmad server"]
pub async fn server(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let _ = msg.channel_id.broadcast_typing(&ctx.http).await;
    let response = match request_info() {
        Ok(A2SInfo { map, current_players, max_players, }) => Cow::from(format!( "**Map:** {}\t**Players:** {}/{}\t**Join:** steam://connect/37.122.208.248:27017", map, current_players, max_players)),
        Err(RequestFailure::CreateQuerier) |
        Err(RequestFailure::RequestError) => Cow::from("My phone is broken. I can't look it up 😟"),
        Err(RequestFailure::MalformedResponse) => Cow::from("This senile old server is spouting gibberish again. \
                                                    I have no idea what it just said 🤔"),
        Err(RequestFailure::Timeout) => Cow::from("FM server isn't picking up the phone. RUDE 😑"),
    };
    msg.channel_id.say(&ctx.http, response).await?;
    Ok(())
}

enum RequestFailure {
    CreateQuerier,
    RequestError,
    Timeout,
    MalformedResponse,
}

fn request_info() -> Result<A2SInfo, RequestFailure> {
    let querier = SteamQuerier::new(FECKINMAD_SERVER).map_err(|_| RequestFailure::CreateQuerier)?;
    querier.get_server_info().map_err(|e| match e {
        QueryError::MalformedResponse => RequestFailure::MalformedResponse,
        QueryError::Socket(_) => RequestFailure::RequestError,
        QueryError::Timeout => RequestFailure::Timeout,
    })
}
